import { BrowserModule } from "@angular/platform-browser";
import { NgModule } from "@angular/core";
import { HttpClientModule } from "@angular/common/http";
import { AppRoutingModule } from "./app.routing.module";
import { FormsModule } from "@angular/forms";

import { AppComponent } from "./app.component";
import { NumerosComponent } from "./numeros/numeros.component";
import { FrasesComponent } from "./frases/frases.component";

@NgModule({
  declarations: [AppComponent, NumerosComponent, FrasesComponent],
  imports: [BrowserModule, HttpClientModule, AppRoutingModule, FormsModule],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {}
