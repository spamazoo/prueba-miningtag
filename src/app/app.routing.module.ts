import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { FrasesComponent } from './frases/frases.component';
import { NumerosComponent } from './numeros/numeros.component';


const routes: Routes = [
    {
        path: 'Numeros',
        component: NumerosComponent,
    },
    {
        path: 'Frases',
        component: FrasesComponent,
    },
    {
        path: '**',
        component: NumerosComponent
    }
];

@NgModule({
    imports: [
        RouterModule.forRoot(routes)
    ],
    exports: [
        RouterModule
    ],
    declarations: []
})
export class AppRoutingModule { }