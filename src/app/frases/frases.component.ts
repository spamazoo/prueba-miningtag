import { Component, OnInit } from "@angular/core";
import { DataService } from "../services/data.service";
import { IRemoteResponse } from "../models/IRemoteResponse";
import { IFrase } from "../models/IFrase";

@Component({
  selector: "app-frases",
  templateUrl: "./frases.component.html",
  styleUrls: ["./frases.component.css"]
})
export class FrasesComponent implements OnInit {
  ABC = "a,b,c,d,e,f,g,h,i,j,k,l,m,n,ñ,o,p,q,r,s,t,u,v,w,x,y,z";
  arrABC: string[] = [];
  frases: IFrase[] = [];
  isLoading = false;
  isError = false;

  constructor(private _dataService: DataService) {}

  ngOnInit() {
    this.arrABC = this.ABC.split(",");
  }

  buscarFrases() {
    this.frases = [];
    this.isLoading = true;
    this.isError = false;
    this._dataService.getFrases().subscribe(
      (resp: IRemoteResponse) => {
        if (resp.success) {
          resp.data.forEach(frase => {
            let quantityLetras = [];
            this.arrABC.forEach(letra => {
              quantityLetras.push(
                frase.paragraph.toLowerCase().split(letra).length - 1
              );
            });

            const obj: IFrase = {
              frase: frase.paragraph.toLowerCase(),
              quantity: quantityLetras,
              sum: quantityLetras.reduce((a, b) => a + b, 0)
            };
            this.frases.push(obj);
          });
          console.log(this.frases);
        } else {
          // Handle error
          this.frases = [];
          this.isError = true;
          console.log(resp.error);
        }
        this.isLoading = false;
      },
      (err: any) => {
        console.log(err);
        this.frases = [];
        this.isError = true;
        this.isLoading = false;
      }
    );
  }
  limpiar() {
    this.frases = [];
    this.isError = false;
  }
}
