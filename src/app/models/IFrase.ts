export interface IFrase {
    frase: string;
    quantity: number[];
    sum: number;
  }
  