export interface INumero {
    number: number;
    quantity: number;
    firstPosition: number;
    lastPosition: number;
  }
  