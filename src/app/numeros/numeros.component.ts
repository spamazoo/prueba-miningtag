import { Component, OnInit } from "@angular/core";
import { DataService } from "../services/data.service";
import { IRemoteResponse } from "../models/IRemoteResponse";
import { INumero } from "../models/INumero";
import { findStaticQueryIds } from "@angular/compiler";

@Component({
  selector: "app-numeros",
  templateUrl: "./numeros.component.html",
  styleUrls: ["./numeros.component.css"]
})
export class NumerosComponent implements OnInit {
  constructor(private _dataService: DataService) {}
  numeros: INumero[] = [];
  numerosOrdenados: string = "";
  isLoading = false;
  isError = false;

  ngOnInit() {}

  buscarNumeros() {
    this.isLoading = true;
    this.isError = false;
    this.numerosOrdenados = "";
    this.numeros = [];
    let numerosDistintos: number[] = [];
    this._dataService.getNumeros().subscribe(
      (resp: IRemoteResponse) => {
        if (resp.success) {
          resp.data.forEach(e => {
            let firstPosition = -1;
            let lastPosition = -1;
            let quantity = 0;
            // Define Columnas
            for (let i = 0; i < resp.data.length; i++) {
              if (resp.data[i] === e) {
                quantity += 1;
                if (lastPosition < i) {
                  lastPosition = i;
                  if (firstPosition == -1) {
                    firstPosition = i;
                  }
                }
              }
            }
            // Verifica que el numero ya exista en el arreglo
            const busqueda = this.numeros.filter(x => x.number == e);
            if (busqueda.length == 0) {
              numerosDistintos.push(e);
              const numero: INumero = {
                number: e,
                quantity: quantity,
                firstPosition: firstPosition,
                lastPosition: lastPosition
              };
              this.numeros.push(numero);
            } else {
              // Ya existe, actualiza valores
              busqueda[0].quantity = quantity;
              busqueda[0].firstPosition = firstPosition;
              busqueda[0].lastPosition = lastPosition;
            }

            this.ordenaNumeros(numerosDistintos);
          });
        } else {
          // Handle error
          this.numeros = [];
          this.isError = true;
        }
        this.isLoading = false;
      },
      (err: any) => {
        console.log(err);
        this.isLoading = false;
        this.isError = true;
        this.numeros = [];
      }
    );
  }

  ordenaNumeros(data: number[]) {
    let aux = 0;
    for (var i = 1; i < data.length; i++) {
      for (var j = 0; j < data.length - i; j++) {
        if (data[j] > data[j + 1]) {
          aux = data[j + 1];
          data[j + 1] = data[j];
          data[j] = aux;
        }
      }
    }

    this.numerosOrdenados = data.join(",");
  }
  limpiar() {
    this.numerosOrdenados = "";
    this.numeros = [];
    this.isError = false;
  }
}
