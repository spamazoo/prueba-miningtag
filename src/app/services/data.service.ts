import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";

@Injectable({
  providedIn: "root"
})
export class DataService {
  URL = "http://168.232.165.184/prueba/";

  constructor(private http: HttpClient) {}

  getNumeros() {
    return this.http.get(`${this.URL}array`);
  }
  getFrases() {
    return this.http.get(`${this.URL}dict`);
  }
}
